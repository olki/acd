package kz.aitu.week6;

import kz.aitu.additional.stack.Stack;
import kz.aitu.week3_cs03.Student;

public class ThreeStackTask {

    public static void main(String[] args) {
        Stack stack1 = new Stack<Integer>();
        Stack stack2 = new Stack<Integer>();
        Stack stack3 = new Stack<Integer>();
        Stack stack4 = new Stack<Integer>();

        stack1.push(1);
        stack1.push(2);
        stack1.push(3);
        stack1.push(4);
        stack1.push(8);

        stack2.push(2);
        stack2.push(3);
        stack2.push(4);
        stack2.push(5);

        stack3.push(3);
        stack3.push(4);
        stack3.push(5);
        stack3.push(6);
        stack3.push(7);

        task1(stack1, stack2, stack3, stack4);

        stack4.print();
    }

    private static void task1(Stack stack1, Stack stack2, Stack stack3, Stack stack4) {

        System.out.println("stack1 top: " + stack1.getTop());
        System.out.println("stack2 top: " + stack2.getTop());
        System.out.println("stack3 top: " + stack3.getTop());

        Integer max = maximum((Integer)stack1.getTop(), (Integer)stack2.getTop(), (Integer)stack3.getTop());

        while(max != null) {
            System.out.println("==============");
            System.out.println(max.intValue());
            System.out.println(((Integer) stack1.getTop()).intValue());

            if (max.intValue() == ((Integer) stack1.getTop()).intValue()) stack1.pop();
            else if (max.intValue() == ((Integer) stack2.getTop()).intValue()) stack2.pop();
            else if (max.intValue() == ((Integer) stack3.getTop()).intValue()) stack3.pop();

            if(stack4.getTop() != max) stack4.push(max);
            System.out.println(stack4.getTop());

        }
    }

    private static Integer maximum(Integer a, Integer b, Integer c) {
        if(a > b && a > c) return a;
        else if(b > a && b > c) return b;
        return c;
    }
}
