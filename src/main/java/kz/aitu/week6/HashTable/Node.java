package kz.aitu.week6.HashTable;

public class Node {

    private String key;
    private String value;
    private Node next;

    public String toString() {
        return "key: " + key + ", value: " + value;
    }

    public boolean equals(Node o) {
        if(this == o) return true;

        if(this.key == o.key) return true;

        return false;
    }
}
