package kz.aitu.week_3.linkedlist_example;

import lombok.Data;

@Data
public class Block {
    private int value;
    private Block nextBlock;
}
