package kz.aitu.week_3.cs02ls;

public class Block {
    private int value;
    private Block nextBlock;

    public Block(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Block getNextBlock() {
        return nextBlock;
    }

    public void setNextBlock(Block n) {
        this.nextBlock = n;
            }
}
