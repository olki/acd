package kz.aitu.week4;

public class Queue {

    private Node head;
    private Node tail;
    private int size;

    public Queue() {
        head = null;
        tail = null;
        size = 0;
    }

    //add()- This method is used to add elements at the tail of queue. More specifically, at the last of linked-list if it is used, or according to the priority in case of priority queue implementation.
    public void add(String data) {
        Node node = new Node(data);

        size++;
        if (this.empty()) {
            this.head = node;
            this.tail = node;
        } else {
            tail.setNext(node);
            tail = node;
        }
    }


    //peek()- This method is used to view the head of queue without removing it. It returns Null if the queue is empty.
    public String peek() {
        return head.getData();
    }


    //poll()- This method removes and returns the head of the queue. It returns null if the queue is empty.
    public String poll() {
        if(head == null) return null;
        Node oldHead = head;
        head = head.getNext();
        size--;
        return oldHead.getData();
    }


    //size()- This method
    public int size() {
        return 0;
    }

    public boolean empty() {
        return head == null;
    }





}
