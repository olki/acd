package kz.aitu.week4;

public class Main {

    public static void main(String[] args) {



        Queue queue = new Queue();

        queue.add("Iphone");
        queue.add("Samsung");
        queue.add("Nokia");
        queue.add("Xiaomi");
        queue.add("Huawei");

        System.out.println(queue.size());//5
        System.out.println(queue.poll());//IPHONE
        System.out.println(queue.peek());//SAMSUNG
        System.out.println(queue.poll());//SAMSUNG
        System.out.println(queue.poll());//NOKIA
        System.out.println(queue.size());//2
        System.out.println(queue.empty());//false
    }
}
