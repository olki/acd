package kz.aitu.sort.quiz8;

public class List {

    private Student[] studentList;
    private int size = 0;

    public List() {
        studentList = new Student[150];
    }

    public void add(Student student) {
        studentList[size++] = student;
    }

    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.println(i + ": " + studentList[i]);
        }
    }

    public void swap(int a, int b) {
        Student temp = studentList[a];
        studentList[a] = studentList[b];
        studentList[b] = temp;
    }

        //type: name_a name_l age gps
    //order: asc desc
    public void sort(String type, String order) {
        int n = size;
        for (int i = 1; i < n; ++i) {
            Student key = studentList[i];
            int j = i - 1;

            while (j >= 0 && compareTo(type, order, key, studentList[j]) > 0) {
                studentList[j + 1] = studentList[j];
                j = j - 1;
            }
            studentList[j + 1] = key;
        }
    }

    public int compareTo(String type, String order, Student a, Student b) {
        long aa = 0;
        long bb = 0;
        int isAsc = 1;
        if(order.equals("asc")) isAsc = -1;

        if(type.equals("name_a")) {
            int size = Math.min(a.getName().length(), b.getName().length());
            for (int i = 0; i < size; i++) {
                aa = a.getName().charAt(i);
                bb = b.getName().charAt(i);
                if(aa != bb) break;
            }
        } else if(type.equals("name_l")) {
            aa = a.getName().length();
            bb = b.getName().length();
        } else if(type.equals("age")) {
            aa = a.getAge();
            bb = b.getAge();
        } else if(type.equals("gpa")) {
            aa = Math.round(a.getGpa() * 100);
            bb = Math.round(b.getGpa() * 100);
        }

        if(aa > bb) return isAsc;
        else if(bb > aa) return isAsc * -1;
        else return 0;
    }
}
