package kz.aitu.additional.graph;

import lombok.Data;

import java.util.Hashtable;

@Data
public class Graph<Key, Value> {

    private boolean biDirectional = false;
    private Hashtable<Key, Vertex<Key, Value>> vertexTable;

    public void addVertex(Key key, Value value) {
        Vertex vertex = new Vertex<Key, Value>(key, value);

        if(vertexTable.containsKey(key)) return;
        else vertexTable.put(key, vertex);
    }

    public void addEdge(Key key1, Key key2) {
        Vertex vertexA = vertexTable.get(key1);
        Vertex vertexB = vertexTable.get(key2);

        vertexA.addEdge(vertexB);
        if(biDirectional) vertexB.addEdge(vertexA);
    }
}
