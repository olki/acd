package kz.aitu.additional.linkedList;

public class LinkedList {

    private Node head;
    private Node tail;


    public void add(String data) {
        Node node= new Node();
        node.setData(data);
        node.setNext(null);

        if(tail == null) {
            head = node;
            tail = node;
        } else {
            tail.setNext(node);
        }
        tail=node;
    }
}
