package kz.aitu.additional.bst;

public class Main {

    public static void main(String[] args) {
        BSTree bsTree = new BSTree();

        bsTree.insert(1000, "A");
        bsTree.insert(2000, "B");
        bsTree.insert(500, "C");
        bsTree.insert(1500, "D");
        bsTree.insert(750, "E");
        bsTree.insert(250, "F");
        bsTree.insert(625, "G");
        bsTree.insert(1250, "H");
        bsTree.insert(875, "I");
        bsTree.insert(810, "Z");

        System.out.println(bsTree.find(750));
        bsTree.printAllAscending();
        bsTree.delete(750);
        bsTree.printAllAscending();

    }
}
