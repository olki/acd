package kz.aitu.additional.bonus;

import javafx.scene.effect.Light;

import java.util.TreeMap;

public class BonusPoints {

    private HashTable hashTable;

    public BonusPoints() {
        hashTable = new HashTable(50);
    }

    public void addRoot(String name) {
        Node node = hashTable.get(name);
        if(node == null) hashTable.set(name);
    }

    public void addChild(String child, String parent) {
        Node nodeP = hashTable.get(parent);
        if(nodeP == null) {
            System.err.println("Parent: " + parent + " not found!");
            return;
        }
        Node nodeCh = hashTable.get(child);
        if(nodeCh != null) {
            System.err.println("Child: " + child + " is already done!");
            return;
        }
        nodeCh = new Node(child);
        if(nodeP.addNode(nodeCh)) {
            hashTable.set(child);
            System.out.println("Child: " + child + " has been added");
        } else {
            System.err.println("Parent: " + parent + " is already two children!");
            return;
        }
        System.out.println(hashTable.get("Student1"));
    }

    public int getPoints(String name) {
        Node node = hashTable.get(name);
        if(node == null) {
            return 0;
        }
        return 80 + getPoints(node, 1);
    }

    private int getPoints(Node node, int level) {
        if(node == null) return 0;

        int point = 0;
        switch (level) {
            case 1: point = 5; break;
            case 2: point = 3; break;
            case 3: point = 2; break;
            default: point = 1;
        }

        return getPoints(node.getLeft(), level + 1)
                + getPoints(node.getRight(), level + 1)
                + point;
    }
}
