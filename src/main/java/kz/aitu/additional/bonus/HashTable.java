package kz.aitu.additional.bonus;

public class HashTable {
    private Node table[];
    private int size;

    public HashTable(int size) {
        table = new Node[size];
        this.size = size;
    }

    public void set(String name) {
        int index = name.hashCode() % size;
        if (table[index] == null) {
            table[index] = new Node(name);
        } else {
            for (Node current = table[index]; current != null; current = current.getNext()) {
                if (current.getName().equals(name)) current.setName(name);
                else if (current.getNext() == null) {
                    current.setNext(new Node(name));
                }
            }
        }


    }


    public Node get(String name) {
        if(name == null) return null;
        int index = name.hashCode() % size;
        for (Node current = table[index]; current != null; current = current.getNext()) {
            if (current.getName().equals(name)) {
                return current;
            }
        }
        return null;
    }
}
